////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Sketch to take a sequence of photos, one every 30 seconds.
////////////////////////////////////////////////////////////////////////////////

// set the number of seconds between shots.
#define SECONDS 300
#define FOCUSBEEPS 2

// Define which outputs are connected to what...
// Define which outputs are connected to what...
#define SPEAKER 9      //  This is connected to one end of a piezo sounder, we oscilate it manually to play sounds 
#define FOCUS 10        //  Set this high to close the relay which focusses
#define SHUTTER 11      //  Set this high to close the relay which triggers the shutter
#define MUTE 0

////////////////////////////////////////
// Called when we power up
void setup() {
  
  // Initialise the pins we're using...
  pinMode(FOCUS, OUTPUT);
  pinMode(SHUTTER, OUTPUT);
  pinMode(SPEAKER, OUTPUT);
  
  // We make sure all switches are turned off before we start
  do_switch(FOCUS, LOW);
  do_switch(SHUTTER, LOW);
  digitalWrite(SPEAKER, LOW);
  
  // Make a couple of FOCUSBEEPS to tell everyone we're underway.
  if(MUTE ==0) {
    secondbeep();
    secondbeep();
  }
}

////////////////////////////////////////
// Called repeatedly
void loop() {
  // First we delay for the specified number of seconds, but the last nn seconds are beeped out...
  delayseconds(SECONDS - FOCUSBEEPS);
  

  // We now tell the camera to focus, we'll allow five seconds to do so...
  focus();
  
  for(int i=0; i < FOCUSBEEPS; i++) {
    secondbeep();
  }
  
  // Time to take the photo
  shoot();
}

////////////////////////////////////////
// Delay for a period defined in seconds
void delayseconds(int seconds) {
  for(int i =0; i< seconds; i++) {
    delay(1000);
  }
  return;
}

////////////////////////////////////////
// Simply make a short beep sound for half a second, followed by a delay of half a second
void secondbeep() {
  int duration = 100;
  // Make the speaker play a tone for half a second...
  if(MUTE == 1) {
    duration = 20;
  } else {
    duration = 100;
  }
  tone(SPEAKER, 1912, duration);
  // And wait a second
  delay(1000);
  return;
}

////////////////////////////////////////
// Tells the camera to focus
void focus() {
  do_switch(FOCUS, HIGH);
  return;
}

////////////////////////////////////////
// Tells the camera to actually take a shot
void shoot() {
  
  // Switch on the second switch, wait half a second then switch both off...  
  do_switch(SHUTTER, HIGH);
  delay(200);
  if(MUTE == 0) {
    tone(SPEAKER, 3830, 50);
  }
  do_switch(SHUTTER, LOW);
  do_switch(FOCUS, LOW);
  return;
}

////////////////////////////////////////
// Indirectly addresses a switch state
void do_switch(int ID, int state) {
  digitalWrite(ID, state);
  return;
}
