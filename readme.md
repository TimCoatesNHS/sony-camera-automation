Sony Camera Automation
======================

Version: 0.1
Date: 01/01/2014
Author: Tim Coates
License: GPL V3

__Please Note: Your mileage may vary, this comes with no guarantees.__


Overview
--------

This is a combination of hardware and software hacks to program a Sony DSLR camera (or I believe Konica Minolta) to take a shot every 30 (programmable) seconds.   

The hardware is based on a Shrimp (see http://shrimping.it/blog/), with modifications where necessary.   

NB: The components selected, and their values are not necessarily 'recommended', I frankly don't know much about electronics, but they do 'seem' to work okay.   

Basic description
-----------------
NB: As an aside, the Sony (Inherited from Konica Minolta) remote control has 3 connections:   
* Common   
* Focus   
* Shutter   

The 'Focus' and 'Shutter' connections are at about +3V in normal use, 'Common' is at 0V. In order to make the camera focus, you need to connect the 'Focus' connection to the 'Common' connection pulling the 'Focus' down to 0V. To take a shot, you also connect the 'Shutter' connection to 'Common', pulling this down to 0V too.

The principle of this project is to use an ATMEGA328 to trigger two transistors to make the necessary connections, at a timed interval.   

The ATMEGA has 3 channels in use:   
Channel 9 (PIN 15) is used for the buzzer.   
Channel 10 (PIN 16) is used for the Focus control.   
Channel 11 (PIN 17) is used for the Shutter control.   

The buzzer is simply controlled using the tone(); command as in: __tone(SPEAKER, 1912, duration);__

The focus and shutter controls are set high to make them work, this does two things. Firstly it lights an LED, mainly so that when I started building this I could see that it might eventually work.  The second thing it does is to power up an NPN transistor to act as a switch.   

The arduino sketch (code) is included in this repository, all as GPL V3 code, please see the included license file!


Parts List
----------
__I'm sure I've missed some parts!__

ATMEGA328 Microprocessor chip, does all the hard work.   
Piezo sounder, to allow annoying beeps with each shot.   
2 x Green LEDs (yep, other colours should work okay), to indicate what's happening.   
2 x BC547 NPN Transistors, to act as switches for the focus and shutter circuits.   
4 x 100 Ohm resistors to reduce the LED / transistor loads.   
1 x Crystal.      
1 x 100nF Capacitor, to smooth the reset signal.   
1 x 10 KOhm resistor, to pull up the reset pin.   
2 x 170 point breadboards.   
1 x Minolta Remote Control shutter switch (Used for it's connector and wire only.).   
1 x 3 x AAA battery holder (with handy switch).   
1 x Old Bluetooth Handsfree unit, ripped apart to use the rechargeable (via USB __:-)__ ) battery.   


TODO LIST
=========
* Create / upload a usable circuit diagram.   
* Simplify the circuit (I think I can route things better).   
*-  DONE - I've moved the focus and shutter to digital outs on the RIGHT of the ATMEGA rather than those on the LEFT.   
*-  DONE - I've flipped one of the output transistor / LED pairs,so they share a common GND line.   
*-  DONE - (see http://youtu.be/f9-Q_Mi9H0M) Test this out, I've got a few shots of the kitchen, but nothing 'time lapse' as yet.   
* Implement some better wiring between this and the original remote shutter switch, so that part can still be used.   
*-  As part of that, add a plug / socket, rather than having wires plugged in.   
* OPTIONALLY - migrate it onto some stripboard, and pack it into a suitable case.
* Investigate Sleep modes for the Arduino, to save on power during the 30 second wait periods.   

Sources
=======
http://goo.gl/iqioBb : Make a Wired SONY ALPHA DSLR Remote (by Brad Justinen) - For wiring details   
http://goo.gl/A7Z03T : Shrimping It Memory Game Expansion Pack - From which I started building   
http://goo.gl/h6cVL : Using the Transistor as a Switch - Transistor Switching - (Told you I don't now about Electronics)   
http://goo.gl/AiS4pU : Sleeping Arduino - Part 4 Wake Up Via Internal Timer   

Changelog
=========
Edited the focus beeps to be shorter and less of them, as battery life (with junk old batteries admittedly) was an issue.
Doubled up the resistors, to reduce power drain through the 2 LEDs and transistors.
Added a 10 second option.

Licensing:
==========
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.   

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.   

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.   